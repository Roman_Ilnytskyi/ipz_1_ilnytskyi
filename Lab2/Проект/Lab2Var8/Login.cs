﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2Var8
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                this.Hide();
                AccountantMenu accountant = new AccountantMenu();
                accountant.Show();
                 
            }
            else
            {
                this.Hide();
                WorkerMenu user = new WorkerMenu();
                user.Show();
            }
        }

        private void RegButton_Click(object sender, EventArgs e)
        {

            this.Hide();
            Registration register = new Registration();
            register.Show();
        }
    }
}
